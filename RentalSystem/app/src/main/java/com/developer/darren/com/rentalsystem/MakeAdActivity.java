package com.developer.darren.com.rentalsystem;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.developer.darren.com.rentalsystem.data.Constants;
import com.developer.darren.com.rentalsystem.model.Advertisement;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.ConsoleHandler;

public class MakeAdActivity extends AppCompatActivity {

    private EditText productNameET, descriptionET, priceET;
    private Spinner catSpinner;
    private Button submitBtn;
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference catRef = database.getReference(Constants.KEY_CATEGORY);
    DatabaseReference adRef = database.getReference(Constants.KEY_ADS);
    List<String> catList;
    ArrayAdapter<String> catAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_make_ad);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        priceET = findViewById(R.id.price_make_ad);
        productNameET = findViewById(R.id.productname_make_ad);
        descriptionET = findViewById(R.id.description_make_ad);
        catSpinner = findViewById(R.id.cat_spinner_make_ad);
        submitBtn = findViewById(R.id.submitBtn_make_ad);
        catList = new ArrayList<>();
        catAdapter = new ArrayAdapter<>(getApplicationContext(),android.R.layout.simple_spinner_item,catList);
        catSpinner.setAdapter(catAdapter);

        catRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                catList.clear();
                for(DataSnapshot ds:dataSnapshot.getChildren()){
                    catList.add(ds.getValue(String.class));
                    catAdapter.notifyDataSetChanged();
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });

        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String userID = Constants.CURRENT_USER.getUserID();
                final String category = catList.get(catSpinner.getSelectedItemPosition());
                final String productName = productNameET.getText().toString();
                final String description = descriptionET.getText().toString();
                final float price = Float.parseFloat(priceET.getText().toString());
                final int[] cnt = {0};
                final String[] adID = new String[1];
                adRef.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                       // Toast.makeText(getApplicationContext(),"key: "+dataSnapshot.getKey()+" child: "+dataSnapshot.getChildrenCount(),Toast.LENGTH_SHORT).show();
                        cnt[0] = (int) dataSnapshot.getChildrenCount();
                        adID[0] = "ad"+(cnt[0]+1);
                        Advertisement a = new Advertisement(description,userID, adID[0],category,productName,price);
                        adRef.push().setValue(a).addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                                Toast.makeText(getApplicationContext(),"Ad placed",Toast.LENGTH_SHORT).show();
                            }
                        })
                                .addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception e) {
                                        Toast.makeText(getApplicationContext(),e.getMessage(),Toast.LENGTH_SHORT).show();
                                    }
                                });
                    }
                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                    }
                });
            }
        });
    }

}
