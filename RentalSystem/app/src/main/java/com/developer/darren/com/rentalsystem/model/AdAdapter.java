package com.developer.darren.com.rentalsystem.model;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.developer.darren.com.rentalsystem.R;
import com.developer.darren.com.rentalsystem.data.Constants;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.TaskExecutors;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.List;

public class AdAdapter extends RecyclerView.Adapter<AdAdapter.MyViewHolder> {

    private Context mContext;
    private List<Advertisement> adList;
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference trRef = database.getReference(Constants.KEY_TRANSACTION);

    public AdAdapter(Context mContext, List<Advertisement> adList) {
        this.mContext = mContext;
        this.adList = adList;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{

        private TextView productNameTV,  sellerTV, descriptionTV,category,price;
        private Button buyButton;

        public MyViewHolder(View itemView) {
            super(itemView);
            productNameTV = itemView.findViewById(R.id.product_name);
            sellerTV = itemView.findViewById(R.id.ad_user_id);
            descriptionTV = itemView.findViewById(R.id.description);
            category=itemView.findViewById(R.id.category);
            price=itemView.findViewById(R.id.price);
            buyButton=itemView.findViewById(R.id.buy_button);
        }
    }

    @Override
    public AdAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.ad_view, parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(AdAdapter.MyViewHolder holder, int position) {
        Log.i("adapter list size"," "+adList.size());
        final Advertisement a = adList.get(position);
        holder.descriptionTV.setText(a.getDescription());
        holder.sellerTV.setText(a.getUserID());
        holder.category.setText(a.getCategory());
        holder.price.setText(String.valueOf(a.getPrice()));
        holder.productNameTV.setText(a.getProductName());
        final User user = Constants.CURRENT_USER;
        if(user==null){
            holder.buyButton.setVisibility(View.GONE);
        }
        else{
            holder.buyButton.setVisibility(View.VISIBLE);
            holder.buyButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    final String adID = a.getAdID();
                    final String buyer = user.getUserID();
                    final String seller = a.getUserID();
                    final String[] trID = { "" };
                    final float amount = a.getPrice();
                    final int[] cnt = {0};
                    trRef.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            cnt[0] = (int) dataSnapshot.getChildrenCount();
                            trID[0] ="tr"+(cnt[0]+1);
                            Transaction tr = new Transaction(trID[0],buyer,seller,adID,amount);
                            trRef.push().setValue(tr).addOnSuccessListener(new OnSuccessListener<Void>() {
                                @Override
                                public void onSuccess(Void aVoid) {
                                    Toast.makeText(mContext,"Transaction success",Toast.LENGTH_SHORT).show();
                                }
                            })
                            .addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                    Toast.makeText(mContext,"Transaction failed",Toast.LENGTH_SHORT).show();
                                }
                            });
                        }
                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return adList.size();
    }
}
